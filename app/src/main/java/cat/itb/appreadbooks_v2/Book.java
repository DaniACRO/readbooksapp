package cat.itb.appreadbooks_v2;

import android.os.Parcel;
import android.os.Parcelable;

public class Book implements Parcelable{
    private String title;
    private String author;
    private String status;
    private float rate;

    /*Constructor de un libro con estatus read*/
    public Book(String title, String author, String status, float rate) {
        this.title = title;
        this.author = author;
        this.status = status;
        this.rate = rate;
    }

    /*Constructor de un libro sin estatus read*/
    public Book(String title, String author, String status) {
        this.title = title;
        this.author = author;
        this.status = status;
    }

    public Book(){

    }

    protected Book(Parcel in) {
        title = in.readString();
        author = in.readString();
        status = in.readString();
        rate = in.readFloat();
    }


    public static final Creator<Book> CREATOR = new Creator<Book>() {
        @Override
        public Book createFromParcel(Parcel in) {
            return new Book(in);
        }

        @Override
        public Book[] newArray(int size) {
            return new Book[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(author);
        dest.writeString(status);
        dest.writeFloat(rate);
    }
}
