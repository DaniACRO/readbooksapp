package cat.itb.appreadbooks_v2;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;


public class BookAdapter extends RecyclerView.Adapter<BookAdapter.BooksViewHolder> {
    List<Book> listBooks;


    public BookAdapter(List<Book> listBooks) {
        this.listBooks = listBooks;
    }


    public class BooksViewHolder extends RecyclerView.ViewHolder{
        private TextView titleBook;
        private TextView authorName;
        private TextView status;
        private RatingBar rating;
        private TextView ratingTitle;


        public BooksViewHolder(@NonNull View itemView) {
            super(itemView);
            titleBook = itemView.findViewById(R.id.bookTitle_ItemtextView);
            authorName = itemView.findViewById(R.id.authorName_ItemTextView);
            status = itemView.findViewById(R.id.status_selected);
            rating = itemView.findViewById(R.id.starRatingBar);
            ratingTitle = itemView.findViewById(R.id.rateStar_Title);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    NavDirections listToFragmentDirections = BookListFragmentDirections.actionListToFragment(listBooks.get(getAdapterPosition()));
                    Navigation.findNavController(v).navigate(listToFragmentDirections);
                }
            });
        }



        public void bind(Book book){
            titleBook.setText(book.getTitle());
            authorName.setText(book.getAuthor());
            status.setText(book.getStatus());
            if (!status.getText().toString().equalsIgnoreCase("read")){
                rating.setVisibility(View.INVISIBLE);
                ratingTitle.setVisibility(View.INVISIBLE);
            }else{
                rating.setVisibility(View.VISIBLE);
                ratingTitle.setVisibility(View.VISIBLE);
                rating.setRating(book.getRate());
            }
        }
    }




    @Override
    public BooksViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.book_item_list, parent, false);
        return new BooksViewHolder(v);
    }


    @Override
    public void onBindViewHolder(@NonNull BooksViewHolder holder, int position) {
        Book book = listBooks.get(position);
        holder.bind(book);
    }


    @Override
    public int getItemCount() {
        return listBooks.size();
    }


}
