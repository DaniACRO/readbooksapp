package cat.itb.appreadbooks_v2;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;


public class BookAddNewFragment extends Fragment {
    private EditText titleBookEditText;
    private EditText authorEditText;
    private Spinner statusSpinner;
    private Button addButton;
    private RatingBar starsBar;
    private Book book;
    private TextView titleFragment;



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.add_new_book_fragment, container, false);

        titleBookEditText = v.findViewById(R.id.bookTitle_editText);
        authorEditText = v.findViewById(R.id.author_editText);
        statusSpinner = v.findViewById(R.id.status_spinner);
        addButton = v.findViewById(R.id.add_button);
        starsBar = v.findViewById(R.id.ratingBar);
        titleFragment = v.findViewById(R.id.title_textView);

        final ArrayAdapter<CharSequence> statusAdapter = ArrayAdapter.createFromResource(getContext(), R.array.status, android.R.layout.simple_spinner_item);
        statusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        statusSpinner.setAdapter(statusAdapter);


        if (getArguments() != null){
            book = getArguments().getParcelable("book");
        }
        if (book != null){
            titleBookEditText.setText(book.getTitle());
            authorEditText.setText(book.getAuthor());

            if (book.getStatus().equalsIgnoreCase("read")){
                statusSpinner.setSelection(2);
            }else if (book.getStatus().equalsIgnoreCase("want to read")){
                statusSpinner.setSelection(0);
            }else{
                statusSpinner.setSelection(1);
            }

            starsBar.setRating(book.getRate());
            addButton.setVisibility(View.INVISIBLE);
            titleFragment.setText(R.string.update);
        }else{
            book = new Book();
        }


        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.add_button:
                        if (titleBookEditText.getText().toString().isEmpty() || authorEditText.getText().toString().isEmpty()){
                            Toast.makeText(getContext(), "Campos obligatorios vacíos", Toast.LENGTH_SHORT).show();
                        }else{
                            if (statusSpinner.getSelectedItemPosition() == 2){
                                book = new Book(titleBookEditText.getText().toString(), authorEditText.getText().toString(), statusSpinner.getSelectedItem().toString(), starsBar.getRating());
                            }else{

                                book = new Book(titleBookEditText.getText().toString(), authorEditText.getText().toString(), statusSpinner.getSelectedItem().toString());
                            }

                            BookViewModel.listBooks.add(book);
                            Navigation.findNavController(v).navigate(R.id.action_list_to_fragment_back);
                        }
                }
            }
        });

        return v;
    }




//    Modificar los datos ya existentes
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (titleBookEditText != null){
            titleBookEditText.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    book.setTitle(titleBookEditText.getText().toString());
                    return false;
                }
            });
        }


        if (authorEditText != null){
            authorEditText.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    book.setAuthor(authorEditText.getText().toString());
                    return false;
                }
            });
        }


        if (statusSpinner != null){
            statusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    book.setStatus(statusSpinner.getSelectedItem().toString());
                    if (statusSpinner.getSelectedItem().toString().equalsIgnoreCase("read")){
                        starsBar.setEnabled(true);
                    }else{
                        starsBar.setEnabled(false);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    book.setStatus(statusSpinner.getSelectedItem().toString());
                }
            });
        }


        if (starsBar != null){
            starsBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                    book.setRate(starsBar.getRating());
                }
            });
        }
    }
}
