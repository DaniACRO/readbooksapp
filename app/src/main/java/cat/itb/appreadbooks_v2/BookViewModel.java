package cat.itb.appreadbooks_v2;

import androidx.lifecycle.ViewModel;
import java.util.ArrayList;
import java.util.List;


public class BookViewModel extends ViewModel {
    static List<Book> listBooks = new ArrayList<Book>();
    String[] statusBook = {"Want to read", "Reading...", "Read"};

    String[] booksTitle = {"Don Quijote de la Mancha", "Romeo y Julieta", "La Divina Comedia", "Cien años de soledad",
    "La Celestina", "Nada", "Mujercitas", "Conversación en La Catedral", "1984", "Sentido y Sensibilidad"};

    String[] bookAuthor = {"Miguel de Cervantes", "Shakespeare", "Dante Alighieri", "Gabriel García Márquez",
    "Fernando de Rojas", "Carmen Laforet", "Louisa May Alcott", "Mario Vargas Llosa", "George Orwell", "Jane Austen"};


    public BookViewModel() {
        for (int i = 0; i < 10; i++){
            int status = (int)(Math.random()*3);
            if (status == 2){
                float rate = (float) (Math.random()*6);
                listBooks.add(new Book( booksTitle[i], bookAuthor[i], statusBook[status], rate));
            }else{
                listBooks.add(new Book(booksTitle[i], bookAuthor[i], statusBook[status]));
            }
        }
    }


    public List<Book> getListBooks() {
        return listBooks;
    }
}
